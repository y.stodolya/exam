﻿// Exam20.02.20 task4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
// Часть 2. Обработка массивов

#include <iostream>
using namespace std;

int main()
{
	int k;
	int n = 0;
	cout << "Enter array range" << endl;
	cin >> n;

	int** arr = new int* [n];
	for (int i = 0; i < n; i++)
		arr[i] = new int[n];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			cin >> arr[i][j];

	for (int i = 0; i < n; i++) {
		k = arr[i][n - i - 1];
		for (int j = 0; j < n; j++) {
			arr[i][j] = arr[i][j]+k;  
		}
	}

	cout << endl;

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}

	delete[] arr;
 
}


