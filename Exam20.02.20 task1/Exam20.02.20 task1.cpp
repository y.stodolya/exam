﻿// Exam20.02.20 task1.cpp 
//Часть 1. задача 1. Найти сумму всех чисел кратных трем диапазона [1,50]

#include <iostream>
using namespace std;

int main()
{
	int i, n;
	int result;
	i = 1;
	n = 50;
	result = 0;
	do {
		if (i % 3 == 0)
			result = result + i;
		i++;
	} while (i <= n);
	cout << result;
	return 0;
}

