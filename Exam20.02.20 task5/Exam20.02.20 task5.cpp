﻿// Exam20.02.20 task5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//Часть 3. Обработка динамических структур

#include <iostream>
#include "QueueStruct.h"
using namespace std;

int main()
{
	TNode* begin = NULL;
	TNode* end = NULL;
	TNode* temp = begin;

	int d;
	int option = 0;

	for (; option != 5;) {
		cout << "Menu" << endl;
		cout << "1 - Add Element" << endl;
		cout << "2 - Show Elements" << endl;
		cout << "3 - Del Element" << endl;
		cout << "4 - Del all" << endl;
		cout << "5 - Sum" << endl;
		cout << "6 - Exit" << endl;
		cout << "Enter option (1-6)" << endl;
		cin >> option;

		switch (option) {
		case 1:
			cin >> d;
			begin == NULL ? create(&begin, &end, d) : add(&end, d); break;
		case 2:
			show(&begin); break;
		case 3:
			del(&begin); break;
		case 4:
			while (begin != NULL)
				del(&begin); break;
		case 5:
			sumeven(&begin); break;
		case 6:
			break;
		default:
			cout << "Incorrect number entered" << endl;
		}

	}

}


/*// Exam20.02.20 task5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//Часть 3. Обработка динамических структур

#include <iostream>
using namespace std;
struct TNode {
	int d;      //поле данных
	TNode* p;   //указатель на связанный элемент
};

void create(TNode** begin, TNode** end, int d) {
	*begin = new TNode;
	(*begin)->d = d;
	(*begin)->p = NULL;
	*end = *begin;
}

void add(TNode** end, int d) {
	TNode* pv = new TNode;
	pv->d = d;
	pv->p = NULL;
	(*end)->p = pv;				// связывание нового элемента с последним 
	*end = pv;					// перестановка указателя конца очереди 
}

int del(TNode** begin) {
	int temp = (*begin)->d;       // извлечение данных   
	TNode* pv = *begin;           // сохранить адрес начала очереди   
	*begin = (*begin)->p;         // переставить указатель начала очереди   
	delete pv;                    // освободить память   
	return temp;
}

void show(TNode** begin) {
	TNode* pv = *begin;
	while (pv != NULL) {
		cout << pv->d << " ";
		pv = pv->p;
	}
}

void sumeven(TNode** begin) {
	int sum = 0;
	TNode* pv = *begin;
	while (pv != NULL) {
		int temp = pv->d;
		if (temp % 2 == 0) {
			sum = sum + temp;
		}
		pv = pv->p;
	}
	cout <<"Sum of even integer: "<< sum;
}

int main()
{
	TNode* begin = NULL;
	TNode* end = NULL;
	TNode* temp = begin;

	int d;
	int option = 0;

	for (; option != 5;) {
		cout << "Menu" << endl;
		cout << "1 - Add Element" << endl;
		cout << "2 - Show Elements" << endl;
		cout << "3 - Del Element" << endl;
		cout << "4 - Del all" << endl;
		cout << "5 - Sum" << endl;
		cout << "6 - Exit" << endl;
		cout << "Enter option (1-6)" << endl;
		cin >> option;

		switch (option) {
		case 1:
			cin >> d;
			begin == NULL ? create(&begin, &end, d) : add(&end, d); break;
		case 2:
			show(&begin); break;
		case 3:
			del(&begin); break;
		case 4:
			while (begin != NULL)
				del(&begin); break;
		case 5:
			sumeven(&begin); break;
		case 6:
			break;
		default:
			cout << "Incorrect number entered" << endl;
		}

	}

}*/