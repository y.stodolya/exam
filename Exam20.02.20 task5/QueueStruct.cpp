#include <iostream>
#include "QueueStruct.h"
using namespace std;


void create(TNode** begin, TNode** end, int d) {
	*begin = new TNode;
	(*begin)->d = d;
	(*begin)->p = NULL;
	*end = *begin;
}

void add(TNode** end, int d) {
	TNode* pv = new TNode;
	pv->d = d;
	pv->p = NULL;
	(*end)->p = pv;
	*end = pv;
}

int del(TNode** begin) {
	int temp = (*begin)->d;
	TNode* pv = *begin;
	*begin = (*begin)->p;
	delete pv;
	return temp;
}

void show(TNode** begin) {
	TNode* pv = *begin;
	while (pv != NULL) {
		cout << pv->d << " ";
		pv = pv->p;
	}
}

void sumeven(TNode** begin) {
	int sum = 0;
	TNode* pv = *begin;
	while (pv != NULL) {
		int temp = pv->d;
		if (temp % 2 == 0) {
			sum = sum + temp;
		}
		pv = pv->p;
	}
	cout << "Sum of even integer: " << sum;
}