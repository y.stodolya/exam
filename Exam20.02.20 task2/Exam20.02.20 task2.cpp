﻿// Exam20.02.20 task2.cpp 
// Часть 1. Задание 2.

#include <iostream>
using namespace std;

int main()
{
	int n, k = 1;
	double sum = 0.0;
	cout << "Enter n" << endl;
	cin >> n;
	while (k <= n) {
		sum = sum + (pow(-1, k+1)/(k*(k+1)));
		k++;
	}
	cout << sum << endl;
}

